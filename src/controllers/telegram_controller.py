##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: telegram_controller.py
# Capitulo: Estilo Microservicios
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Version: 3.0.0 Febrero 2022
# Descripción:
#
#   Ésta clase define el controlador del microservicio API. 
#   Implementa la funcionalidad y lógica de negocio del Microservicio.
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |     send_message()     |         Ninguno          |  - Procesa el mensaje |
#           |                        |                          |    recibido en la     |
#           |                        |                          |    petición y ejecuta |
#           |                        |                          |    el envío a         |
#           |                        |                          |    Telegram.          |
#           +------------------------+--------------------------+-----------------------+
#
#-------------------------------------------------------------------------
from flask import request, jsonify
from configparser import ConfigParser
import telepot
import json

class TelegramController:

    @staticmethod
    def _init_():
        configur = ConfigParser()
        configur.read('/notifier/settings.ini')
        token = configur.get('TELEGRAM', 'TOKEN')
        chat_id = configur.get('TELEGRAM', 'CHAT_ID')
        bot = telepot.Bot(token)
        return bot, chat_id

    @staticmethod
    def send_message():
        data = json.loads(request.data)
        if not data:
            return jsonify({"msg": "Accion no valida"}), 400
        msg = data["message"]
        bot, chat_id = TelegramController._init_()
        if bot.sendMessage(chat_id, msg):
            return jsonify({"msg": "El mensaje fue enviado"}), 200
        else:
            return jsonify({"msg": "El mensaje no se pudo enviar"}), 500